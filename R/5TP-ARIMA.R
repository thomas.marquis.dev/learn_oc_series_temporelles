install.packages('tidyverse')
library(tidyverse)

install.packages('tsibble')
library(tsibble)

install.packages('lubridate')
library(lubridate)

install.packages('forecast')
library(forecast)

library(modelr)

install.packages('latex2exp')
library(latex2exp)

install.packages('caschrono')
library(caschrono)

# saisonalité
season <- 12
# retard considéré dans les sorties ACF & PACF
n_lag <- 36

data <- AirPassengers %>%
  as_tsibble %>%
  mutate(t = index, passengers = value, log_pass=log(passengers))
  
data %>%
  ggplot() +
  geom_line(aes(x = t, y = log_pass))

# autocorrélogramme simple si la série avait été stationnaire

acf_data <- acf(data$log_pass, lag.max = n_lag, plot = FALSE)
acf_data$lag <- acf_data$lag * season

as_acf <- function(acf_res) {
  acf_res %>%
    as_tibble %>%
    rename(ACF = V1) %>%
    rowid_to_column('h')
}

acf_data$acf %>%
  as_acf %>%
  ggplot() +
  geom_col(aes(x = h, y = ACF))

# on remarque que cette sortie ACF a une décroissance lente :
# ce qui l'empêche d'être un autocorrélogramme simple (qui est sencé
# arriver rapidement à 0)

# On va donc différentier la série une fois pour tenter de la rendre
# stationnaire :

data_diff_1 <- data %>%
  mutate(diff_log_pass = difference(log_pass, lag = 1, differences = 1)) %>%
  filter(!is.na(diff_log_pass))

acf_diff <- acf(data_diff_1$diff_log_pass, lag.max = n_lag, plot = FALSE)
acf_diff$lag <- acf_diff$lag * season

acf_diff$acf %>%
  as_acf %>%
  ggplot() +
  geom_col(aes(x = h, y = ACF)) +
  ylim(-1, 1)

# on voit des pique tous les 12
# on fait donc une différentiation d'ordre 12 :

data_diff_1_12 <- data_diff_1 %>%
  mutate(diff_log_pass_12 = difference(diff_log_pass, lag = 12, differences = 1)) %>%
  filter(!is.na(diff_log_pass_12))

acf_diff_12 <- acf(data_diff_1_12$diff_log_pass_12, lag.max = n_lag, plot = FALSE)
acf_diff_12$lag <- acf_diff_12$lag * season

acf_diff_12$acf %>%
  as_acf %>%
  ggplot() +
  geom_col(aes(x = h, y = ACF)) +
  ylab(TeX('$\\widehat{\\rho}(h)$')) +
  ylim(-1, 1)

# cette fois-ci, c'est bon !
# on peut donc considérer que ACF = rho_chapeau(h)
# et on peut calculer r_chapea(h) avec la fonction PACF :

pacf_diff_12 <- pacf(data_diff_1_12$diff_log_pass_12, lag.max = n_lag, plot = FALSE)
pacf_diff_12$lag <- pacf_diff_12$lag * season

pacf_diff_12$acf %>%
  as_acf %>%
  ggplot() +
  geom_col(aes(x = h, y = ACF)) +
  ylab(TeX('$\\widehat{r}(h)$')) +
  ylim(-1, 1)
